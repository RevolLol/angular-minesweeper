/**
 * generates a random number in a given range
 * @param {Number} nMax 
 * @param {Number} nMin 
 */
function getRandNum(nMax, nMin){
	return Math.round(Math.random() * (nMax - nMin) + nMin)
}

function Field(nRows, nCols){
	this.field = Field.generateField(nRows, nCols);
	this._mineRatio = 0.15;
	this.mineCounts = new Array(nRows).fill().map(_ => new Array(nCols).fill());
	/**
	 * generating first mine locations before user clicks on any cell
	 * if we guessed correctly, we wont't have to waste time generating mines when user actually makes the first click
	 */
	let bGameFinished = false, bIsSuccess = false, aMineLocations = [], nCellsLeft = 0;

	this.isGameFinished = function(){
		return bGameFinished
	}

	this.isGameSuccess = function(){
		return bGameFinished && bIsSuccess
	}

	/**
	 * @param {Number} nRow
	 * @param {Number} nCol
	 */
	this.openCell = function(nRow, nCol){
		//checking if given coordinates even belong to the playing field
		if(nRow >= this.field.length || nCol >= this.field[0].length)
			throw new Error(`Wrong cell coordinates ${nRow}, ${nCol} given`);
			
		//also checking if that cell hasnt been opened yet and game hasnt ended yet
		if(this.mineCounts[nRow][nCol] === undefined && !bGameFinished) {
			//checking if game was started
			if(!aMineLocations.length) {
				aMineLocations = this.genMineLocations(this.getNearCells(nRow, nCol).concat([[nRow, nCol]]));
				aMineLocations.forEach(aCoords => this.field[aCoords[0]][aCoords[1]].setMine());
				
				nCellsLeft = nRows * nCols - aMineLocations.length;
				console.log(`initial cells to open: ${nCellsLeft}`);
			}
			const aCellsToOpen = [[nRow, nCol]], aChangedCells = [];
			for(var i = 0; i < aCellsToOpen.length && i < nCellsLeft; i++) {
				var [rowToOpen, colToOpen] = aCellsToOpen[i];
				aChangedCells.push(aCellsToOpen[i]);
				if(this.field[rowToOpen][colToOpen].open()) {
					this.mineCounts[rowToOpen][colToOpen] = this.countNearMines(rowToOpen, colToOpen);
					//open other mines around, if no mines were found here
					if(this.mineCounts[rowToOpen][colToOpen] === 0) {
						const aNextCells = this.getNearCells(rowToOpen, colToOpen).filter(aCoords => {
							var [checkRow, checkCol] = aCoords;
							//not even bothering to check cached cell for current iteration if this cell was opened previously
							if(this.mineCounts[checkRow][checkCol] === undefined) {
								return !aCellsToOpen.some(aCachedCoords => aCachedCoords[0] == aCoords[0] && aCachedCoords[1] == aCoords[1]);
							}
							return false
						});
						aCellsToOpen.push(...aNextCells);
					}
				}
				else {
					this.mineCounts[nRow][nCol] = -1;
				}
			}
			console.log(`cells to open: ${aCellsToOpen.length}`);
			console.log(`changed cells: ${aChangedCells.length}`);
			//if only 1 cell with original coordinates from function call was changed and it was a mine, the game's done
			if(i === 1 && this.mineCounts[nRow][nCol] === -1) {
				bGameFinished = true;
				bIsSuccess = false;
			}
			else {
				nCellsLeft -= aChangedCells.length;
				console.log(`cells left to open: ${nCellsLeft}`);
				//checking if all regular cells are opened and mines are either marked or closed
				bGameFinished = nCellsLeft === 0 && aMineLocations.every(aCoords => this.field[aCoords[0]][aCoords[1]].getState() !== 1);
				bIsSuccess = bGameFinished;
			}
				
			return aChangedCells
		}
		return []
	}

	this._getMines = () => aMineLocations;
}

/**
 * generates a 2-d array that acts as a playing field
 * @param {Number} nRows 
 * @param {Number} nCols 
 * @returns {Array}
 */
Field.generateField = function(nRows, nCols){
    if(nRows > 0 && nCols > 0) {
		const aField = new Array(nRows);
		for(let i = 0; i < nRows; i++) {
			aField[i] = new Array(nCols);
			for(let j = 0; j < nCols; j++) {
				aField[i][j] = new Cell();
			}
		}

		return aField
    }
    throw new Error("Number of rows and columns both must be greater than 0");
}

/**
 * 
 * 
 * 
 */
Field.prototype.getNearCells = function(nRow, nCol){
	let aCells = [],
		/**
		 * if clicked cell is on the the first row (0 index), we take it as a starting row, 
		 * otherwise we take a previous row
		 */
		nStartRow = Math.max(nRow - 1, 0),
		/**
		 * if clicked cell is on the the last row, we take it as an end row, 
		 * otherwise we take the next one
		 */
		nEndRow = Math.min(nRow + 1, this.field.length - 1),
		nStartCol = Math.max(nCol - 1, 0),
		nEndCol = Math.min(nCol + 1, this.field[0].length - 1);
	for(let i = nStartRow; i <= nEndRow; i++) {
		for(let j = nStartCol; j <= nEndCol; j++) {
			aCells.push([i,j]);
		}
	}
	//removing the mine at given coords itself from resulting array
	aCells.splice((nEndCol - nStartCol + 1) * (nRow - nStartRow) + nCol - nStartCol, 1);
	return aCells
}

/**
 * counts how many mines surround the given cell
 * @param {Number} nRow
 * @param {Number} nCol
 * @returns {Number}
 */
Field.prototype.countNearMines = function(nRow, nCol){
	return this.getNearCells(nRow, nCol).reduce((nMines, aCell) => {
		return this.field[aCell[0]][aCell[1]].hasMine() ? ++nMines : nMines
	}, 0);
}

Field.prototype.getMinesNumber = function(){
	return Math.round(this.field.length * this.field[0].length * this._mineRatio)
}

/**
 * returns an array of [x,y] coordinates to place mines to
 * TODO make proper generation (right now it just randomly distributes mines over all cells with no awareness)
 * @param {number[][]} aExcludeCoords - excludes pairs of cell coodrdinates before mine generation starts
 * @returns {Array}
 */
Field.prototype.genMineLocations = function(aExcludeCoords) {
	const nRows = this.field.length, 
		nCols = this.field[0].length,
		aMinesCoords = new Array(this.getMinesNumber()),
		aExclude = new Array(nRows);
	for(let i = 0; i < nRows; i++) {
		aExclude[i] = new Array(nCols).fill().map((_, nInd) => nInd);
	}

	if(aMinesCoords.length > nRows * nCols - aExcludeCoords.length) {
		throw new Error(`${aMinesCoords.length} mines cannot be placed with those rate or/and excluded cells on a field [${nRows}, ${nCols}]`);
	}

	aExcludeCoords.forEach(aCoords => {
		let aRow = aExclude[aCoords[0]];
		aRow.splice(aRow.indexOf(aCoords[1]), 1);
	});

	for(let i = 0; i < aMinesCoords.length; i++) {
		let nRowInd = getRandNum(0, aExclude.length - 1),
			nColInd = getRandNum(0, aExclude[nRowInd].length - 1);
		aMinesCoords[i] = [nRowInd, aExclude[nRowInd][nColInd]];
		//excluding generated coordinates from possible options for further generations
		aExclude[nRowInd].splice(nColInd, 1);
		//checking if there are available mine-free cells left in this row
		if(aExclude[nRowInd].length === 0) {
			aExclude.splice(nRowInd, 1);
		}
	}
	return aMinesCoords
}

Field.prototype.markCell = function(nRow, nCol){
	const oCell = this.field[nRow][nCol]; 
	let nNewState = oCell.getState();
	if(oCell.getState() !== 1) {
		nNewState = nNewState ^ 2;
		//using xor to mark or remove mark from cell, cuz xoring a value against itself will result in a 0
		oCell.setState(nNewState);
	}
	return nNewState;
}

/**
 * cell states: 0 - closed, 1 - opened, 2 - marked
 * isMine - indicates if cell has a mine or not
 */
 
function Cell() {
	let nState = 0, bisMine = false;

	this.getState = () => nState;

	this.setState = function(nNewState) {
		if(nNewState >= 0 && nNewState < 3) {
			nState = nNewState;
			return this
		}
		throw new Error("State has to be a number in range of [0,2]");
	}

	this.hasMine = () => bisMine;

	this.setMine = () => bisMine = true;
}

Cell.prototype.open = function(){
	if(this.getState() === 0) {
		this.setState(1);
	}
	return !this.hasMine()
}

Cell.prototype.markMine = function(){
	const nState = this.getState();
	//checking if cell wasnt yet opened
	if(nState !== 1) {
		this.setState(nState ? 0 : 2);
		return this
	}
}

Cell.states = ["closed", "opened", "marked"];
