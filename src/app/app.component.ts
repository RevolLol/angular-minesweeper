import { Component, ComponentRef, ViewChild } from '@angular/core';
import { PopupService } from './dialog/popupservice.service';
import { PopupdlgComponent } from './dialog/popupdlg/popupdlg.component';
import { TextComponent } from "@components/index";
import { ComponentInjector } from './misc/componentInjector';
import { FieldDirective } from "./fieldDirective";
import { FieldComponent } from "./game/field/field.component";
import { ResultsComponent } from "./game/results/results.component";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Angular playground';
  textToDisplay = "Im a cool text component. Just like that button down there";
  @ViewChild(FieldDirective, { static: true }) gameField: FieldDirective;
  rows: number = 6;
  cols: number = 10;

  private dlgInst: ComponentRef<PopupdlgComponent>;

  constructor(private popupService: PopupService, private injector: ComponentInjector) { }

  onFieldCreate(): void {
    const gameField: ComponentRef<FieldComponent> = this.injector.createComponent(FieldComponent);
    const resDialog: ComponentRef<ResultsComponent> = this.injector.createComponent(ResultsComponent);
    this.dlgInst = this.popupService.getPopupDlg();
    this.dlgInst.instance.headerText = "The game is finished!";
    this.dlgInst.instance.class= "resultsDlg";
    this.dlgInst.instance.setDlgBody(resDialog);
    this.dlgInst.instance.buttons = [
      {
        text: "Close",
        enabled: true,
        type: "Default",
        onclick: () => {
          this.dlgInst.instance.close();
        }
      }
    ];

    gameField.instance.rows = this.rows;
    gameField.instance.cols = this.cols;

    var restartSub = resDialog.instance.restart.subscribe(() =>{
      gameField.instance.initGame();
      this.dlgInst.instance.close();
    }, null, () => {
      restartSub.unsubscribe();
    });

    var gameEndSub = gameField.instance.gameFinish.subscribe(resultData => {
      resDialog.instance.success = resultData.success;
      resDialog.instance.clicksMade = resultData.clicksMade;
      resDialog.instance.secondsPassed = resultData.secondsPassed;
      
      this.dlgInst.instance.open();
    }, null, () => {
      gameEndSub.unsubscribe();
    });
    this.gameField.viewContainerRef.insert(gameField.hostView);
  }

}
