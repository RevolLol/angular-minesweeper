import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PopupdlgComponent } from './popupdlg/popupdlg.component';
import { DlgDirective } from './dlgDirective.directive';
import { PopupService } from './popupservice.service';
import { MessageToastComponent } from './message-toast/message-toast.component';
import { ComponentlibModule } from '@components/componentlib.module';

@NgModule({
  declarations: [
    DlgDirective,
    PopupdlgComponent,
    MessageToastComponent
  ],
  imports: [
    CommonModule,
    ComponentlibModule
  ],
  providers: [
    PopupService
  ]
})
export class DialogModule { }
