import { ComponentRef, Injectable } from '@angular/core';
import { PopupdlgComponent } from './popupdlg/popupdlg.component';
import { MessageToastComponent } from '@dialog/message-toast/message-toast.component';
import { ComponentInjector } from '@app/misc/componentInjector';


@Injectable()
export class PopupService {
  constructor(private injector: ComponentInjector) {

  }

  // Previous dynamic-loading method required you to set up infrastructure
  // before adding the popup to the DOM.
  getPopupDlg():ComponentRef<PopupdlgComponent>  {
    // Create element
    const dlgNode = document.createElement('app-popupdlg');
    
    const dialogComponent:ComponentRef<PopupdlgComponent> = this.injector.createComponent(PopupdlgComponent, dlgNode);

    return dialogComponent;
  }

  showPopup(text:string, duration?:number, autoclose?:boolean):ComponentRef<MessageToastComponent>{
    const msgToastNode = document.createElement('app-message-toast');
    const msgToastComponent:ComponentRef<MessageToastComponent> = this.injector.createComponent(MessageToastComponent, msgToastNode);
    msgToastComponent.instance.text = text;
    msgToastComponent.instance.duration = duration || msgToastComponent.instance.duration; 
    msgToastComponent.instance.onClose.subscribe(() => {
      msgToastComponent.destroy();
    });
    if(autoclose === false) {
      msgToastComponent.instance.autoclose = autoclose;
      return msgToastComponent;
    }
    msgToastComponent.instance.open();
    return null
  }
}