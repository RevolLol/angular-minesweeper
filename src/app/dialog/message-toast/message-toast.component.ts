import { Component, OnInit, Input, ViewRef, ApplicationRef, ElementRef } from '@angular/core';
import { BasicPopup } from '../basicPopup.component';

@Component({
  selector: 'app-message-toast',
  templateUrl: './message-toast.component.html',
  styleUrls: ['./message-toast.component.css']
})
export class MessageToastComponent extends BasicPopup implements OnInit {

  @Input() text: string;
  @Input() duration: number = 3000;
  @Input() autoclose: boolean = true;

  constructor(viewRef:ViewRef, appRef:ApplicationRef, elementRef:ElementRef) {
		super(viewRef, appRef, elementRef);
  }
  
  open(): void {
    super.open();
    if(this.autoclose) {
      window.setTimeout(() => {
        this.close();            
      }, this.duration);
    }
  }

  ngOnInit(): void {
  }

}
