import { Component, OnInit, Input, Output, EventEmitter, OnDestroy, ViewChild, ComponentRef, ViewRef, ApplicationRef, ElementRef } from '@angular/core';
import { BasicPopup } from '@app/dialog/basicPopup.component';
import { ButtonComponent } from '@components/button/button.component';
import { DlgDirective } from '../dlgDirective.directive';

interface ButtonMeta {
	text:string,
	enabled?:boolean,
	onclick(),
	type?: string,
	icon?:string,
	iconURI?:string,
}

@Component({
	selector: 'app-popupdlg',
	templateUrl: './popupdlg.component.html',
	styleUrls: ['./popupdlg.component.css']
})
export class PopupdlgComponent extends BasicPopup implements OnInit, OnDestroy {

	@Input() icon: string;
	@Input() iconURI: string;
	@Input() headerText: string;
	@ViewChild(DlgDirective, { static: true }) dlgBody: DlgDirective;
	@Input() buttons: ButtonMeta[];
	@Input() class: string;

	constructor(viewRef:ViewRef, appRef:ApplicationRef, elementRef:ElementRef) {
		super(viewRef, appRef, elementRef);
	}

	ngOnDestroy(): void {
	}

	ngOnInit(): void {
	}

	setDlgBody(component: ComponentRef<any>) {
		this.dlgBody.viewContainerRef.clear();
		this.dlgBody.viewContainerRef.insert(component.hostView);
	}

}
