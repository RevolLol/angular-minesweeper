import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PopupdlgComponent } from './popupdlg.component';

describe('PopupdlgComponent', () => {
  let component: PopupdlgComponent;
  let fixture: ComponentFixture<PopupdlgComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PopupdlgComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PopupdlgComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
