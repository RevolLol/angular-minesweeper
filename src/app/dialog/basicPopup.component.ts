import { Component, ViewRef, ApplicationRef, ElementRef, Output, EventEmitter } from '@angular/core';

@Component({ template: '' })
export class BasicPopup {
	@Output() onOpen = new EventEmitter<any>();
	@Output() onClose = new EventEmitter<any>();

    constructor(protected viewRef:ViewRef, protected appRef:ApplicationRef, protected elementRef:ElementRef){

    }

    open(): void {
		this.appRef.attachView(this.viewRef);
		// Add to the DOM
		document.body.appendChild(this.elementRef.nativeElement);
		this.onOpen.emit();
	}

	close(): void {
		document.body.removeChild(this.elementRef.nativeElement);
		this.viewRef.detach();
		this.onClose.emit();
	}
}