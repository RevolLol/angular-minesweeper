import { Directive, ViewContainerRef } from '@angular/core';

@Directive({
    selector: '[dlgBodyContainer]',
})

export class DlgDirective {
    constructor(public viewContainerRef: ViewContainerRef) { }
}