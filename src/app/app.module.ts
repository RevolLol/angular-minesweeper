import { BrowserModule } from '@angular/platform-browser';
import { FormsModule }   from '@angular/forms';
import { ComponentlibModule } from "./componentlib/componentlib.module";
import { DialogModule } from "./dialog/dialog.module";
import { GameModule } from './game/game.module';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { ComponentInjector } from './misc/componentInjector';
import { FieldDirective } from './fieldDirective';

@NgModule({
  declarations: [
    AppComponent,
    FieldDirective
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ComponentlibModule,
    DialogModule,
    GameModule
  ],
  providers: [ComponentInjector],
  bootstrap: [AppComponent]
})
export class AppModule { }
