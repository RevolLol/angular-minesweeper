import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CellComponent } from './cell/cell.component';
import { FieldComponent } from './field/field.component';
import { FixDigitFormat } from './pipes/fixDigitFormat.pipe';
import { GameAPIService } from './serviceAPI/gameAPI.service';
import { ResultsComponent } from './results/results.component';
import { ComponentlibModule } from "@components/componentlib.module";


@NgModule({
  declarations: [CellComponent, FieldComponent, FixDigitFormat, ResultsComponent],
  imports: [
    CommonModule,
    ComponentlibModule
  ],
  exports: [
    CellComponent,
    FieldComponent
  ],
  providers: [
    GameAPIService
  ]
})
export class GameModule { }
