declare const Field: any;

import { Injectable } from '@angular/core';
import { Observable, Subject, ReplaySubject } from 'rxjs';
import { scan } from 'rxjs/operators'

interface CellData {
	minesNear?: number;
	state: number;
	isMine?: boolean;
}

@Injectable({
	providedIn: 'root'
})
export class GameAPIService {

	private fieldInstance: any;
	get field() {
		if (!this.field) {
			throw new Error("Field was not initialized");
		}
		return this.field
	}

	private cellSubjects: ReplaySubject<CellData>[][];
	get cellObservables():Observable<CellData>[][] {
		if (!this.fieldInstance) {
			throw new Error("Field was not initialized");
		}
		return this.cellSubjects.map((subRow: Subject<any>[]) => subRow.map(cellSub => cellSub.asObservable()))
	}

	private _gameState: Subject<boolean>;
	get gameState():Observable<boolean> {
		return this._gameState.asObservable();
	}

	/* private _minesLeft:Subject<number>;
	getMinesLeft():Observable<number> {
		return this._minesLeft ? this._minesLeft.asObservable().pipe(
			scan((currCount, newState) => {
				return newState === 2 ? --currCount : ++currCount
			}, this.fieldInstance.getMinesNumber())
		) : null;
	} */

	private _minesUnmarked:number;
	get minesUnmarked():number {
		return this._minesUnmarked
	}

	constructor() {

	}

	initField(rows: number, cols: number) {
		this.fieldInstance = new Field(rows, cols);
		this._gameState = new Subject();
		/* this._minesLeft = new ReplaySubject()
		this._minesLeft.next(this.fieldInstance._getMines().length); */
		this._minesUnmarked = this.fieldInstance.getMinesNumber();
		this.cellSubjects = this.fieldInstance.field.map((row: any[]) => row.map(cell => { 
			const cellSub = new ReplaySubject(); 
			cellSub.next({
				minesNear: -1,
				state: cell.getState(),
				isMine: cell.hasMine()
			});
			return cellSub
		}));
		return this;
	}

	openCell(row: number, column: number): void {
		const changes: Object[] = this.fieldInstance.openCell(row, column);

		//debug code for showing mines
		this.fieldInstance.field.forEach((cellRow, nRow) => {
			cellRow.forEach((cellInst, nCol) => {
				this.nextCellState(nRow, nCol, {
					state: cellInst.getState(),
					isMine: cellInst.hasMine(),
					minesNear: this.fieldInstance.mineCounts[nRow][nCol]
				});
			});
		});

/* 		changes.forEach((aData: any[]) => {
			const [cellRow, cellCol] = aData, cellInst = this.fieldInstance.field[cellRow][cellCol];
			let mineCount: number = this.fieldInstance.mineCounts[cellRow][cellCol];
			this.nextCellState(cellRow, cellCol, {
				state: cellInst.getState(),
				isMine: cellInst.hasMine(),
				minesNear: mineCount
			});
		}); */

		if(this.fieldInstance.isGameFinished()) {
			this._gameState.next(this.fieldInstance.isGameSuccess());
			this._gameState.complete();
		}
	}

	markCell(row: number, column: number) {
		let nState = this.fieldInstance.markCell(row, column);
		this.nextCellState(row, column, { state: nState});
		this._minesUnmarked += nState === 2 ? -1 : 1;
	}

	private nextCellState(row:number, column:number, state:CellData):void {
		const cellInst = this.fieldInstance.field[row][column];
		//if any of the state attributes are missing, we get the current one from the cell itself and add it to cell observable
		this.cellSubjects[row][column].next(Object.assign({}, {
			state: cellInst.getState(),
			isMine: cellInst.hasMine(),
			minesNear: this.fieldInstance.mineCounts[row][column]
		}, state));
	}
}
