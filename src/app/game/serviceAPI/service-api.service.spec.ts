import { TestBed } from '@angular/core/testing';

import { gameAPIService } from './gameAPI.service';

describe('ServiceAPIService', () => {
  let service: gameAPIService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(gameAPIService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
