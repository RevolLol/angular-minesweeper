import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ButtonComponent } from '@components/index';

@Component({
  selector: 'app-results',
  templateUrl: './results.component.html',
  styleUrls: ['./results.component.css']
})
export class ResultsComponent implements OnInit {

  @Input() success:boolean;
  @Input() secondsPassed:number;
  @Input() clicksMade:number;
  @Output() restart:EventEmitter<void> = new EventEmitter<void>();

  constructor() { }

  ngOnInit(): void {
  }

  onStartNew(): void {
    this.restart.emit();
  }

}
