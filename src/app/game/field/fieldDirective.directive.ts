import { Directive, ViewContainerRef } from '@angular/core';

@Directive({
    selector: '[fieldCellContainer]'
})
export class FieldDirective {
    constructor(public viewContainerRef: ViewContainerRef){
        
    }
}