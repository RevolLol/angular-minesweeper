declare const Field: any;
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Observable } from 'rxjs';
import { FixDigitFormat } from '../pipes/fixDigitFormat.pipe';
import { GameAPIService } from '../serviceAPI/gameAPI.service';

interface CellData {
	minesNear: number;
	state: number;
	isMine: boolean;
}

interface ResultData {
	secondsPassed: number;
	clicksMade: number;
	success: boolean;
}

@Component({
	selector: 'app-field',
	templateUrl: './field.component.html',
	styleUrls: ['./field.component.less']
})
export class FieldComponent implements OnInit {

	@Input() rows: number;
	@Input() cols: number;
	@Output() gameFinish = new EventEmitter<ResultData>();
	cells: Observable<any>[][];

	private _secondsPassed:number;
	get secondsPassed() {
		return this._secondsPassed
	}

	private clicksMade:number = 0;
	private timeCounter:number;


	constructor(public gameAPI:GameAPIService) {

	}

	ngOnInit(): void {
		this.initGame();
	}

	openCell(row: number, column: number): void {
		++this.clicksMade;
		if(this.secondsPassed === 0) {
			this.timeCounter = window.setInterval(() => { this._secondsPassed++ }, 1000);
		}
		this.gameAPI.openCell(row, column);
	}

	markCell(row: number, column: number): void {
		this.gameAPI.markCell(row, column);
	}

	initGame(){
		this.gameAPI.initField(this.rows, this.cols);
		this.cells = this.gameAPI.cellObservables;
		window.clearInterval(this.timeCounter);
		this._secondsPassed = 0;
		
		var oSub = this.gameAPI.gameState.subscribe((result:boolean) => {
			this.gameFinish.emit({
				secondsPassed: this.timeCounter,
				clicksMade:this.clicksMade,
				success: result
			});
		}, () => { 
			console.log("an error occurred");
		}, () => {
			oSub.unsubscribe();
		});
	}

}
