import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-cell',
  templateUrl: './cell.component.html',
  styleUrls: ['./cell.component.less']
})
export class CellComponent implements OnInit {

  @Input() minesNear: number;
  @Input() state: number;
  @Input() isMine: boolean;
  @Output() onClick = new EventEmitter<any>();
  @Output() onMark = new EventEmitter<any>();

  private _colors: string[] = [
    "transparent",
    "#0000FF",
    "#008000",
    "#FF0000",
    "#000080",
    "#800000",
    "#008080",
    "#000000",
    "#808080"
  ];

  get colors(): string[] {
    return this._colors
  }

  private _states: string[] = [
    "closed",
    "opened",
    "marked"
  ]

  get states(): string[] {
    return this._states
  }

  constructor() { }

  ngOnInit(): void {
  }

  onCellClick(): void {
    if(this._states[this.state] === "closed") {
      this.onClick.emit();
    }
  }

  onCellMark(event: MouseEvent): void {
    event.preventDefault();
    if(this._states[this.state] !== "opened") {
      this.onMark.emit();
    }
  }

}
