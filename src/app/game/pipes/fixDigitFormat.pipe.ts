import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: "fixDigitFrmt"
})
export class FixDigitFormat implements PipeTransform {
    transform(value: number, numFrmt?: number): string {
        numFrmt = numFrmt || 3;
        //we always take an abs value of the number and account for negative numbers later
        const strVal:string = value ? Math.abs(value).toString() : "", 
            negativeSign:string = value < 0 ? "-" : "",
            diff:number = numFrmt - strVal.length - negativeSign.length;
        if(diff >= 0) {
            return negativeSign + new Array(diff + 1).join("0").concat(strVal)
        }
        return strVal
    }
}