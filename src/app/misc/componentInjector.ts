import { Injectable, Injector, ComponentFactoryResolver, ComponentRef, Component } from '@angular/core';

@Injectable()
export class ComponentInjector {
    constructor(private injector: Injector, private factoryResolver:ComponentFactoryResolver){

    }

    createComponent(componentClass, domRef = null):ComponentRef<any> {
        const factory = this.factoryResolver.resolveComponentFactory(componentClass);
        return domRef ? factory.create(this.injector, [], domRef) : factory.create(this.injector);
    }
}