import { Component, OnInit, OnChanges, SimpleChanges, Input } from '@angular/core';
import { Required, CatalogValue, CheckCatalogValues } from '../decorators/index';

enum casingTypes {
  Capital,
  CapitalFirst,
  Lower,
  AsIs
}

enum alignTypes {
  Begin,
  Center,
  End
}

enum fontSizes {
  default = 1,
  h3 = 1.2,
  h2 = 1.5,
  h1 = 2
}

@Component({
  selector: 'app-text',
  templateUrl: './text.component.html',
  styleUrls: ['./text.component.css']
})
export class TextComponent implements OnInit, OnChanges {

  @Input() text: string;
  @Input() @CatalogValue(alignTypes) textAlign: string;
  @Input() @CatalogValue(casingTypes) casing: string;
  @Input() @CatalogValue(fontSizes) fontSize: string;

  get cssFontSize(): string {
    return (this.fontSize ? fontSizes[this.fontSize] : fontSizes.default) + "em"
  }

  constructor() { }

  @CheckCatalogValues()
  ngOnInit(): void {
    this.casing = this.casing || casingTypes[casingTypes.AsIs];
    let casingValue: any = casingTypes[this.casing];
    if(casingValue == undefined) {
      throw new Error(`Invalid value ${this.casing} set for attribute 'casingValue'`);
    }
  }

  @CheckCatalogValues()
  ngOnChanges(changes: SimpleChanges): void {
    
  }

}
