export function CheckCatalogValues(): MethodDecorator {
    return function (target: Function, propertyKey: string, descriptor: any) {
        const originalMethod = descriptor.value;

        descriptor.value = function (...args: any[]) {
            let failedChecks: string[] = Object.keys(this["propCatalogs"]).filter((propName: string) => {
                let propCatalog: Object = this["propCatalogs"][propName];
                return propCatalog[this[propName]] === undefined
            });
            if(failedChecks.length > 0) {
                throw new Error(failedChecks.map((attrName: string) => `Invalid value '${this[attrName]}' set for attribute '${attrName}'`).join(";\n"));
            }
            return originalMethod.apply(this, args);
        }

        return descriptor;
    }
}