export function CatalogValue(catalog:object): PropertyDecorator {
  return function(target: Object, propertyKey: string) {
    if(target["propCatalogs"] === undefined) {
      target["propCatalogs"] = {};
    }
    //creating immutable object to store catalogs for class properties marked with this decorator
    Object.defineProperty(target["propCatalogs"], propertyKey, {
      writable: false,
      configurable: false,
      enumerable: true,
      value: catalog
    });
  }
}