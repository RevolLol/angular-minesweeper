import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TextComponent } from './text/text.component';
import { ButtonComponent } from './button/button.component';


@NgModule({
  declarations: [
    TextComponent,
    ButtonComponent
  ],
  exports: [
    TextComponent,
    ButtonComponent
  ],
  imports: [
    CommonModule
  ]
})
export class ComponentlibModule { }
