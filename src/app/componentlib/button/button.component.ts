import { Component, OnInit, Input, Output, EventEmitter, SimpleChanges } from '@angular/core';
import { CatalogValue, CheckCatalogValues } from '../decorators/index';

enum ButtonTypes {
  Accept,
  Reject,
  Default,
  Transparent
}

@Component({
  selector: 'app-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.less']
})
export class ButtonComponent implements OnInit {

  @Input() enabled: boolean = false;
  @Input() icon: string;
  @Input() iconURI: string;
  @Input() text: string;
  @Input() @CatalogValue(ButtonTypes) type: string;
  @Output() click = new EventEmitter<void>();
  @Output() dblClick = new EventEmitter<void>();

  clickTimeout: number = -1;

  constructor() { }

  @CheckCatalogValues()
  ngOnInit(): void {
  }

  @CheckCatalogValues()
  ngOnChanges(changes: SimpleChanges): void {
    
  }

  onClick(event: MouseEvent): void {
    if(this.clickTimeout === -1) {
      this.clickTimeout = window.setTimeout(() => {
        window.clearTimeout(this.clickTimeout);
        this.clickTimeout = -1;
        this.click.emit();
      }, 200);
    }
    else {
      window.clearTimeout(this.clickTimeout);
      this.clickTimeout = -1;
    }
    event.stopPropagation();
  }

  onDblClick(event: MouseEvent): void {
    this.dblClick.emit();
    event.stopPropagation();
  }

}
