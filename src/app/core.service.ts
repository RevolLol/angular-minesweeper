import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CoreService {

  static zindexInc: number = 10;

  constructor() { }
}
